window.onload = () => {
    let myText = "Hello, this is my text!";
    let compteur = 0;

    function textFunc() {
        let main = document.getElementById('textClass');
        
        if (compteur < myText.length) {
            text = main.innerHTML.substring(0, main.innerHTML.length - 1);
            text += myText.charAt(compteur) + "_";
            main.innerHTML = text;
        } else {
            main.innerHTML = "";
            compteur = -1;
        }

        compteur++;
        setTimeout(textFunc, 1000);
    }

    textFunc();
}