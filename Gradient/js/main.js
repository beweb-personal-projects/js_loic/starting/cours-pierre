
window.onload = () => {
    let allArray = [];

    let maxCol = 12;
    let maxRow = 20;

    function createTable() {
        let number = 0;
        for(let i = 0; i < maxCol; i++) {
            let tempArray = []
            for(let j = 0; j < maxRow; j++) {
                number = i + "." + j
                tempArray.push(number);
            }
            allArray.push(tempArray);
            tempArray = [];
            number = i + 1;
        }
    }

    // console.log(allArray)
    createTable();
 
    function createUI() {
        let table = document.querySelector('#tableHtml');
        let element = 0;
        for(let i = 0; i < allArray.length; i++) {
            let tr = document.createElement('tr');
            for(let j = 0; j < allArray[i].length; j++) {
                let td = document.createElement('td');
                td.innerText = allArray[i][j];
                td.setAttribute('id', `squares-${element}`)
                tr.appendChild(td)
                element += 1;
            }
            table.appendChild(tr);
        }
    }
    
    createUI();

    function setBackgrounds() {
        let rgb = {r: 0, g: 0, b: 255};
        let changeColorR = rgb.r / (maxCol*maxRow)
        let changeColorB = rgb.b / (maxCol*maxRow)
        let element = 0;

        for(let i = 0; i < allArray.length; i++) {
            for(let j = 0; j < allArray[i].length; j++) {
                let square = document.querySelector(`#squares-${element}`)
                square.style.backgroundColor = `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`
                rgb.r += changeColorR;
                rgb.b -= changeColorB;

                element += 1;
            }
        }
    }
    
    setBackgrounds();
}
