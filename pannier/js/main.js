window.onload = () => {
    const MENU = document.querySelector('.contentmenu_create');
    const CLOSEOPEN = document.querySelector('.closeopen_create');
    let closeopen_status = false
    let allObj = [];

    CLOSEOPEN.addEventListener('click', function(){
        if(closeopen_status) {
            closeopen_status = !closeopen_status;
            MENU.style.display = "none";
            CLOSEOPEN.innerHTML = ">";
        } else {
            closeopen_status = !closeopen_status;
            MENU.style.display = "table";
            CLOSEOPEN.innerHTML = "<";
        }
    })

    const FORM = document.querySelector('.contentcenter_create');

    FORM.addEventListener('submit', function(e) {
        e.preventDefault();
        const FORMDATA = new FormData(e.target);
        createProduct(FORMDATA);
    });

    const createProduct = (formdata) => {
        let product = {
            name: formdata.get("name"),
            reference: formdata.get("reference"),
            quantity: formdata.get("quantity"),
            description: formdata.get("description"),
            price: formdata.get("price")
        };

        insertProduct(product);
    };

    const insertProduct = (product) => {
        let numProducts = localStorage.length;
        localStorage.setItem(`product_${numProducts}`, JSON.stringify(product));

        let tr = document.querySelector('.separators');
        tr.remove();
        
        productsRefresh();
    };

    const productsRefresh = () => {
        let table = document.querySelector('.tablebody');

        for(let i = 0; i < localStorage.length; i++) {
            let currentProduct = JSON.parse(localStorage.getItem(`product_${i}`));
            allObj.push(currentProduct);
        }

        for(let i = 0; i < allObj.length; i++) {
            let tr = document.createElement('tr');
            tr.setAttribute('class', "separators")
            table.appendChild(tr)

            for (const product in allObj[i]) {
                let td = document.createElement('td');
                td.innerHTML = allObj[i][product];
                tr.appendChild(td)
            }

            let input = document.createElement('input');
            input.setAttribute('type', `submit`)
            input.setAttribute('id', `product_${i}`)
            input.setAttribute('class', `inputstyle`)
            tr.appendChild(input)
        }

    }

    productsRefresh();
}


