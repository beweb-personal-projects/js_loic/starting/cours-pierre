window.onload = () => {
    let c = document.getElementById("myScreen");
    let ctx = c.getContext("2d");
    let max_width = 500;

    function createCode() {
        let verif = true;
        let width = 0;
        for(let i = 0; i < max_width; i++) {
            let random = Math.floor(Math.random() * 20)

            if (verif) {
                ctx.fillStyle = 'black';
                ctx.fillRect(width, 0, random, 200);
                max_width -= random;
                width += random;
                verif = !verif;
            } else {
                ctx.fillStyle = 'white';
                ctx.fillRect(random, 0, random, 200);
                max_width -= random;
                width += random;
                verif = !verif;
            }
        }
    }

    createCode();
   
}