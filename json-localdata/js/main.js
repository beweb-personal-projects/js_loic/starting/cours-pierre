window.onload = () => {
    let submit = document.querySelector('#send_data');

    submit.addEventListener('click', function() {
        let name = prompt('Name:');
        let age = prompt('Age:');
        let data = localStorage.getItem('allObjects');
        console.clear();

        if (name === null || age === null || name === "" || age === "") {
            console.log("You cant create an object if your dont enter any type of data!");
        } else {

            let obj = [{
                name: name,
                age: age,
            }]

            if(data === null) {
                let objJSON = JSON.stringify([obj]);
                localStorage.setItem('allObjects', objJSON);
            } else {
                let allObjects = JSON.parse(data);
                allObjects.push(obj);
                let objJSON = JSON.stringify(allObjects);
                localStorage.setItem('allObjects', objJSON);
            }

            console.log(retrieveData());
            console.log(retrieveAgeAverage());
        }
    })

    const retrieveData = () => {
        let data = JSON.parse(localStorage.getItem('allObjects'));

        if (data === null) {
            return false;
        } else {
            let tbl = [];
        
            for(let i = 0; i < data.length; i++) {
                tbl.push(data[i][0]);
            }

            return tbl;
        }
    }

    const retrieveSpecificData = (dataType) => {
        let data = JSON.parse(localStorage.getItem('allObjects'));

        if (data === null) {
            return false;
        } else {
            let tbl = [];
        
            switch (dataType) {
                case 'name':
                    for(let i = 0; i < data.length; i++) {
                        tbl.push(data[i][0].name);
                    }
                    return tbl;
                case 'age':
                    for(let i = 0; i < data.length; i++) {
                        tbl.push(data[i][0].age);
                    }
                    return tbl;
                default:
                    return false;
            } 
        }
    }

    const retrieveDataJSON = () => {
        let data = localStorage.getItem('allObjects');

        if (data === null) {
            return false;
        } else {
            return data;
        }
    }

    const retrieveAgeAverage = () => {
        let allAges = retrieveSpecificData('age');
        let ageAverage = 0;

        if (!Array.isArray(allAges)) {
            return "You don't currently have any type of data to print the Age Average!";
        } else {
            for(let i = 0; i < allAges.length; i++) {
                ageAverage += Number(allAges[i]);
            }
            ageAverage /= allAges.length;
            return "Age Average: " + Math.round(ageAverage * 100) / 100;
        }
    }

    let submit2 = document.querySelector('#send_data2');

    submit2.addEventListener('click', function(e) {
       console.log(this.innerHTML);
       console.log(e.target.innerHTML);
    })

}