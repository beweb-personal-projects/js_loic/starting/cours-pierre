// padrao(vogais, ) / silabas / palavras / frase
window.onload = () => {
    const VOYELLES = ["a", "e", "i", "o", "u", "y"]
    const CONSONNES = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z"];

    const genPhoneme = () => {
        let myVoyelle = VOYELLES[Math.floor(Math.random()*VOYELLES.length)];
        let myConsonne = CONSONNES[Math.floor(Math.random()*CONSONNES.length)];

        let random = Math.random();

        if (random < 0.5) {
            return myVoyelle + myConsonne; 
        } else {
            return myConsonne + myVoyelle;
        }
    }
    
    const genMots = (maxSyllabes) => {
        let myMot = "";

        for(let i = 0; i < maxSyllabes; i++) {
            myMot += genPhoneme();
        }

        return myMot
    }

    const genPhrase = (maxMots, maxSyllabes) => {
        let myPhrase = "";

        for (let i = 0; i < maxMots; i++) {
            myPhrase += genMots(maxSyllabes) + " ";
        }

        return myPhrase;
    }

    console.log(genPhrase(4, 3));
}
